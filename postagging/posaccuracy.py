# !/usr/bin/python3
"""USAGE: python3 accuracy.py <tagged_label> <tagged_output>"""
# calculate accuracy: 
# read correct labels into a list, do the same for classified results
# loop through the list and count the number of identical elements
# accuracy = num_identical / num_output

import os
import sys

def disp_usage():
    print('\nUSAGE: python3 accuracy.py <correct_tagged> <tagged_output>\n')
    return


def cal_accuracy(labelfile, resultfile):
    label = []
    result = []
    identical = 0
    with open(labelfile, 'r', errors='ignore') as f:
        for line in f:
            label += line.split()
    with open(resultfile, 'r') as g:
        for line in g:
            result += line.split()
    for i, lab in enumerate(label):
##        print(lab)
##        print(result[i])
##        print('next test:')
        if lab == result[i]:
            identical += 1
    accuracy = identical / len(label)
    return accuracy


def main():
    try:
       argvlist = list(sys.argv)
       correct_label = argvlist[1]
       classify_result = argvlist[2]
       #pos_cls = argvlist[3]
    except IndexError:
        print('\nUSAGE: python3 accuracy.py <correct_tagged> <tagged_output>\n')
        return
    except OSError:
        disp_usage()
        return
    
    accuracy = cal_accuracy(correct_label, classify_result)

    print('\nComparing result', classify_result, 'to correct tagged', correct_label)
    print('Accuracy =', accuracy, '\n')
    
    
if __name__ == "__main__":
    main()    
