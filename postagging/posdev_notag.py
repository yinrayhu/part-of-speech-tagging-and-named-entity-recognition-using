# !/usr/bin/python
"""Process pos.dev to get words, no POS tags"""

import os
import sys
import codecs
# to avoid UnicodeDecodeError:
sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')

def disp_usage():
    print('\nUSAGE: python3 posdev_notag.py DEVFILE OUTFILE')
    print('e.g. python3 posdev_notag.py ../../data/pos.dev ../../data/pos.dev.notag\n')
    return


def strip_tag(dev, out):
    with open(dev, 'r', errors='ignore') as f, open(out, 'w') as g:
        for line in f:
            inlist = line.split()
            outlist = []
            for item in inlist:
                # use .rsplit() so the script applies to NER as well
                outlist.append(item.rsplit('/',1)[0])
            g.write(' '.join(outlist) + '\n')
    return print('\nstripping complete!\n')

    
def main():
    try:
        infile = sys.argv[1]
        outfile = sys.argv[2]
        strip_tag(infile, outfile)
    except IndexError:
        print('\nERROR: Incorrect # of arguments!')
        disp_usage()
        return
    except OSError:
        print('\nERROR: Make sure your DEVFILE exists!')
        disp_usage()
        return


if __name__ == '__main__':
    main()
