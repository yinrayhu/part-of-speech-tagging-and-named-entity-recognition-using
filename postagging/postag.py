# !/usr/bin/python3
"""Part-of-speech tagger using MODEL, take input from STDIN, and output to STDOUT"""

import sys
import postrain
sys.path.append('../')
import perceplearn, percepclassify


def disp_usage():
    print('\nUSAGE[1]: python3 postag.py MODEL')
    print('USAGE[2]: python3 postag.py MODELFILE < STDIN_file > STDOUT_file\n')
    return


def append_tag(postag, sent):
    """tag a sentence (string called sent) given POS list"""
    words = sent.split()
    if len(postag) != len(words):
        print('#_postags is different from #_words in the sentence!', file=sys.stderr)
    for i in range(len(postag)):
        words[i] += '/' + postag[i]
    return ' '.join(words)


def pos_classify(sent, w_avg, label_list):
    """use percepclassify to determine the tag for each word"""
    # usage of percepclassify: result = classify(test_sample, w_avg, label_list)
    # need to organize 'sent' into sliding window of features
    # return a list of tags
    mylist = sent.split()
    result_tag = [''] * len(mylist)
    if len(mylist) == 1:
            feat = 'Pre_bos Cur_' + mylist[0].lower() + ' Nex_eos'
            result_tag[0] = percepclassify.classify(feat, w_avg, label_list)
            return result_tag
    for i, word in enumerate(mylist):
        # construct a sliding window of feature vectors
        if i == 0:
            feat = 'Pre_bos Cur_'+word.lower()+' Nex_'+mylist[i+1].lower()
            result_tag[i] = percepclassify.classify(feat, w_avg, label_list)
        elif i == len(mylist) - 1:
            feat = 'Pre_'+mylist[i-1].lower()+' Cur_'+word.lower()+' Nex_eos'
            result_tag[i] = percepclassify.classify(feat, w_avg, label_list)
        else:
            feat = 'Pre_'+mylist[i-1].lower()+' Cur_'+word.lower()+' Nex_'+mylist[i+1].lower()
            result_tag[i] = percepclassify.classify(feat, w_avg, label_list)
    return result_tag


def main():
    """use postrain and percepclassify to format, classify, and reformat back, to STDOUT"""
    # similar to percepclassify.py
    # first read model file
    try:
        label_list, feat_space, w_avg = percepclassify.get_weight(sys.argv[1])
    except IndexError:
        disp_usage()
        return
    except OSError:
        disp_usage()
        return
    
    # use postrain to format dev set: need to strip '/' to split word/pos pair
    # save correct pos
    
    test_sample, result = [], []
    while test_sample != '':
        try:
            test_sample = input()
            # result_pos is a list containing pos for each word
            # use percepclassfy
            result_pos = pos_classify(test_sample, w_avg, label_list)
            
            # result_pos needs to append to each word -> create tagged sentence
            result_tagged = append_tag(result_pos, test_sample)
            print(result_tagged)
        except ValueError:
            print('Input value error.', file=sys.stderr)
            return
        except EOFError:
            print('End of input file.', file=sys.stderr)
            return

if __name__ == '__main__':
    main()
