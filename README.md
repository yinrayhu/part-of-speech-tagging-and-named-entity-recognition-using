CSCI 544 HW2 by Yin-Ray Rick Huang

Part I: averaged perceptron

perceplearn.py:

1. Get class names & feature space from input file; 

2. Train model using averaged perceptron; 

3. Output MODELFILE in such format:	CLASS	FEATURE	  WEIGHT; 

4. USAGE: python3 perceplearn.py TRAININGFILE MODELFILE

-- stdout looks like this --

Reading input file. Adding class labels and feature space.

Iteration no. 1; 
Iteration no. 2; 
Iteration no. 3; 
Iteration no. 4; 
Iteration no. 5; 
Iteration no. 6; 
Iteration no. 7; 
Iteration no. 8; 

Model successfully written to ../output/eg1.model

-- Results for Part I using SPAM training and dev set --

Comparing result ../output/spam.stdout to correct labels ../data/SPAM_DEV.label; 

Accuracy = 0.9640498899486427 

Label: HAM - 

Precision = 0.975975975975976; 

Recall = 0.975; 

F-score = 0.9754877438719359; 


Label: SPAM - 

Precision = 0.9313186813186813; 

Recall = 0.9338842975206612; 

F-score = 0.9325997248968363; 


Part II: postrain.py

-- import perceplearn, change pos.train format, output MODEL --

-- use 'Pre_', 'Cur_', 'Nex_' prefix to create features

-- Reformatted training data successfully written to POS_INPUT_TO_PERCEPTRON -- 

Part II: postag.py

-- use postrain and percepclassify to format, classify, and reformat back, to STDOUT --

-- need to append predicted tag to original words to obtain the output file --


Part III: nelearn.py

-- similar to postrain.py, but instead of .split('/'), use .rsplit('/',1) to extract entity--

Part III: netag.py

-- similar to postag.py in tagging, similar to nelearn.py in formatting data --


Part IV: accuracy and f-score

Accuracy for POS tagger:

Comparing result ../../output/pos.dev.out to correct tagged ../../data/pos.dev

--> Accuracy = 0.9357130393598724

Part IV: NER performance

Over all NE:

prescision = 0.7359154929577465

recall = 0.5485564304461942

F-Score = 0.6285714285714284

*****************************************************

For 'PER':

prescision = 0.7049180327868853

recall = 0.5149700598802395

F-Score = 0.5951557093425606

*****************************************************

For 'LOC':

prescision = 0.8157894736842105

recall = 0.6118421052631579

F-Score = 0.6992481203007519

*****************************************************

For 'ORG':

prescision = 0.7647058823529411

recall = 0.5306122448979592

F-Score = 0.6265060240963856

*****************************************************

For 'MISC':

prescision = 0.2857142857142857

recall = 0.3076923076923077

F-Score = 0.29629629629629634

Part IV: discussion

-- nb vs perceptron: if I use 'PrevWord_w1  CurrWord_w2  NextWord_w3' as a feature vector for nblearn, the predicted pos accuracy is compatible to postag.py, with the perceptron superior by 1%. However, if I only use 'CurrWord_w' as the feature for nb, since it is not considering the dependency of ajacent words, the accuracy gap is more obvious. --

-- Why use perceptron? If I use the same feature for nb, the accuracy is almost the same, and perceptron is slower due to iterations, then what's the advantage? Ans: when we have ONLINE learning with ever-increasing training data on limited storage, which is often the case in the real world, perceptron is more feasible than naive bayes. Because perceptron focuses on updating the weight using new samples, the algoritm does not require the whole training set each time a new sample is added. This is not the case for nb, where the whole dataset must be present in order to calculate the probability. -- 


