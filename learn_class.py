# !/usr/bin/python3

"""
get class names & feature space from input line
"""

# label is a list of strings for class names
# label_weight dict records current weight for each label
# label_w_sum is the cumulative weight
# current_result is a dict {'label_name': classify_product}
label_list = []
#label_weight = {}
label_w_sum = {}
current_result = {}
feat_space = []


def read_file(infile):
    with open(infile, 'r') as f:
        for line in f:
            add_label(line.split()[0])
            add_dim(line.split()[1:])

    
def add_label(label):
    global label_list
    if label not in label_list:
        label_list.append(label)


def add_dim(feat_vec):
    # increase feature space dimension if encounter unseen feature
    global feat_space
    for feat in feat_vec:
        if feat not in feat_space: feat_space.append(feat)


class LearnClass:
    # inline is a line from input file
    # self.label is the correct class label from training, y
    # w is the weight vector, dict of weights
    # weights are numbers, keys of dict w are words (features), x
    
    # access LearnClass using a dict of class objects
    # e.g., initialize --> add a class object to dict
    # by mydict[label] = LearnClass()
    # add training samples and update w --> mydict[label].update(inline)

    global label_list
    #global label_weight
    global label_w_sum
    global feat_space
    global current_result
    
    def __init__(self):
        # initialize w by setting all weights to zero
        self.w = {}
        for feat in feat_space:
            self.w[feat] = 0

    def input(self, inline):
        self.label, self.x = inline.split()[0], inline.split()[1:]

    def inner_product(self, inline):
        # return classification result, compare to other,
        # come back to update if needed
        self.input(inline)
        self.z = 0
        for item in self.x:
            #print('WORD:',item,'  WEIGHT:',self.w[item])
            self.z += self.w[item]
        return self.z

    def increase(self, inline):
        self.input(inline)
        for item in self.x:
            self.w[item] += 1

    def decrease(self, inline):
        self.input(inline)
        for item in self.x:
            self.w[item] -= 1
    
    def print_w(self):
        print(self.w)


def pick_class():
    # current_result = {'label_name': inner_product, ...}
    # pick classification result using argmax_i{z_i}
    # if correct, all class w unchanged, add w to label_w_sum
    global label_list
    global current_result

    # pick largest product from classification result,
    # start from 0th label and continue comparison
    argmax = label_list[0]
    for label in label_list[1:]:
        if current_result.get(label) > current_result.get(argmax):
            argmax = label
    return argmax


def main():
    global label_list
    #global label_weight
    global label_w_sum
    global current_result
    global feat_space
    
    infile = '../data/training_eg1.dat'
    read_file(infile)
    # initialize class object dictionary using labels and feature space
    # initialize {'label': {w_sum} } dict of dict 
    classdict = {label: LearnClass() for label in label_list}
    label_w_sum = {label: {} for label in label_list}
    for label in label_w_sum:
        label_w_sum[label] = {feat:0 for feat in feat_space}

    with open(infile, 'r') as f:
        for line in f:
            print('\n',line)
            correct = line.split()[0]
            current_result = {}
            for label in label_list:
                current_result[label] = classdict[label].inner_product(line)
            guess = pick_class()
            
            if guess != correct:
                # the "guess" class should be penalized, "correct" class rewarded
                classdict[guess].decrease(line)
                classdict[correct].increase(line)

            # for AVERAGE perceptron   
            for label in label_list:
                #print('LABEL:',label,'  W:',classdict[label].w)
                for feat in classdict[label].w.keys():
                    label_w_sum[label][feat] += classdict[label].w.get(feat)
                
        print(label_w_sum)


if __name__ == '__main__':
    main()
