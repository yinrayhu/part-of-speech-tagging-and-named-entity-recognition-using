# !/usr/bin/python3
"""USAGE: python3 accuracy.py <correct_label> <classify_output>"""
# calculate accuracy: 
# read correct labels into a list, do the same for classified results
# loop through the list and count the number of identical elements
# accuracy = num_identical / num_output

import os
import sys


def cal_accuracy(labelfile, resultfile):
    label = []
    result = []
    identical = 0
    with open(labelfile, 'r') as f:
        for line in f:
            label.append(line.strip())
    with open(resultfile, 'r') as g:
        for line in g:
            result.append(line.strip())
    for i, lab in enumerate(label):
##        print(lab)
##        print(result[i])
##        print('next test:')
        if lab == result[i]:
            identical += 1
    accuracy = identical / len(label)
    return accuracy


def main():
    try:
       argvlist = list(sys.argv)
       correct_label = argvlist[1]
       classify_result = argvlist[2]
       #pos_cls = argvlist[3]
    except IndexError:
        print('\nUSAGE: python3 accuracy.py <correct_label> <classify_output>\n')
        return
    
    accuracy = cal_accuracy(correct_label, classify_result)

    print('\nComparing result', classify_result, 'to correct labels', correct_label)
    print('Accuracy =', accuracy, '\n')
    
    
if __name__ == "__main__":
    main()    
