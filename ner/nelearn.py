# !/usr/bin/python3
"""NER learning code, similar to postrain.py"""
# postrain_ner -> difference from postrain is .rsplit('/',1)
# and don't do .lower() to avoid losing entity capitalization
import sys
import os
import postrain_ner
sys.path.append('../')
import perceplearn

MAX_ITER = 8

def disp_usage():
    """usage instruction"""
    print('\nUSAGE: python3 nelearn.py TRAININGFILE MODEL\n')
    return


def main():
    """Format NER TRAININGFILE, then similar to postrain.py"""
    # Format TRAININGFILE, call perceplearn.py to train model, write to MODEL
    try:
        TRAININGFILE = sys.argv[1]
        MODEL = sys.argv[2]
        percep_input = postrain_ner.change_format(TRAININGFILE)
    except IndexError:
        disp_usage()
        return
    except OSError:
        print('\nMake sure your TRAININGFILE exists!\n')
        return

    postrain_ner.write_data(percep_input, 'ner_input_for_percep.tmp')
    input_as_list, label_list, feat_space = perceplearn.read_file('ner_input_for_percep.tmp')
    print('\nperceptron training for NER:')
    avg_w = perceplearn.iterate(input_as_list, label_list, feat_space, MAX_ITER)
    perceplearn.write_model(avg_w, MODEL)


if __name__ == '__main__':
    main()
