# !/usr/bin/python3
"""NER training using perceptron from perceplearn; import this in nelearn.py"""
# MAIN DIFFERENCE FROM POSTRAIN: .rsplit('/',1)  -> get NER tag
# instead of just .split('/')

import sys
import os
sys.path.append('../')
import percepclassify, perceplearn

MAX_ITER = 8

def disp_usage():
    """usage instruction"""
    #print('\nUSAGE: python3 postrain_ner.py TRAININGFILE MODEL')
    print('\nThis program is intended for importing in nelearn.py!\n')
    return


def change_format(infile):
    """change infile format such that it can be used by perceplearn.py"""
    # percep_input = {LABEL_1: {'three-word-feat': #_occurance, ...}, ...}
    # add BOS & EOF before the first word and after the last word
    # do the following for each word:
    # split a window of 3 words with -d'/': prev, curr, next
    # NER of curr word as the class label for this sample
    # feat vec is 'Pre_w1  Cur_w2  Nex_w3'
    print('\nchange input file format for perceptron training:')
    percep_input = {}
    with open(infile, 'r', errors = 'ignore') as f:
        for line in f:
            # mylist contains word/pos/ner pairs from each line
            mylist = line.split()
            percep_input = construct_dict(mylist, percep_input)
            #print('format changed for the line starts with', mylist[0])
    print('format change done.\n')
    return percep_input


def make_sample(pre, cur, nex):
    """process an input of 3 word/pos/ner pairs, return [label, feat_vec]"""
    # take the right most item after the last '/' as NER
    # keep WORD/POS as a feature
    Curr, CurLabel = cur.rsplit('/',1)
    Prev, PreLabel = pre.rsplit('/',1)
    Next, NexLabel = nex.rsplit('/',1)

    feat_vec = 'Pre_'+Prev +' Cur_'+Curr +' Nex_'+Next
    # print(feat_vec)
    return CurLabel, feat_vec


def construct_dict(mylist, mydict):
    """augment percep_input dict using split input line and current percep_input"""
    # process input line into sliding windows and call make_sample()
    # print(mylist)
    for i, item in enumerate(mylist):
        if i == 0:
            # use 'BOS' as the previous word of the 1st word in a sentence
            if len(mylist) == 1:
                # also add 'EOS' if the line only contains one word
                pre, cur, nex = 'BOS/BOS', mylist[0], 'EOS/EOS'
            else:
                pre, cur, nex = 'BOS/BOS', mylist[0], mylist[1]
            label, vec = make_sample(pre, cur, nex)
            if label not in mydict.keys():
                mydict[label] = {}
            if vec not in mydict[label].keys():
                mydict[label][vec] = 1
            else:
                mydict[label][vec] += 1
        elif i == len(mylist) - 1:
            # use 'EOS' as the next word of the last word in a sentence
            pre, cur, nex = mylist[i-1], mylist[i], 'EOS/EOS'
            label, vec = make_sample(pre, cur, nex)
            if label not in mydict.keys():
                mydict[label] = {}
            if vec not in mydict[label].keys():
                mydict[label][vec] = 1
            else:
                mydict[label][vec] += 1
        else:
            pre, cur, nex = mylist[i-1], mylist[i], mylist[i+1]
            label, vec = make_sample(pre, cur, nex)
            if label not in mydict.keys():
                mydict[label] = {}
            if vec not in mydict[label].keys():
                mydict[label][vec] = 1
            else:
                mydict[label][vec] += 1
    return mydict


def write_data(percep_input, outfile):
    """write formatted NER training data to outfile"""
    # outfile format, each line looks like:
    # CLASS_LABEL    PREV_WORD    CURR_WORD    NEXT_WORD
    # percep_input = {LABEL_1: {'three-word-feat': #_occurance} }
    with open(outfile, 'w') as f:
        for label in percep_input.keys():
            for feat in percep_input[label].keys():
                # write repeatedly if feat occur more than once
                for i in range(percep_input[label][feat]):
                    f.write(label+' '+feat+'\n')                                
    print('\nReformatted training data successfully written to', outfile, '\n')
    return

    
def main():
    """Format TRAININGFILE, call perceplearn.py to train model, write to MODEL"""
    # shouldn't use this main(), run nelearn.py instead!
    try:
        TRAININGFILE = sys.argv[1]
        MODEL = sys.argv[2]
        percep_input = change_format(TRAININGFILE)
    except IndexError:
        disp_usage()
        return
    except OSError:
        print('\nMake sure your TRAININGFILE exists!\n')
        return

    write_data(percep_input, 'POS_INPUT_TO_PERCEPTRON')
    input_as_list, label_list, feat_space = perceplearn.read_file('POS_INPUT_TO_PERCEPTRON')
    print('\nperceptron training:')
    avg_w = perceplearn.iterate(input_as_list, label_list, feat_space, MAX_ITER)
    perceplearn.write_model(avg_w, MODEL)

    
if __name__ == '__main__':
    main()
